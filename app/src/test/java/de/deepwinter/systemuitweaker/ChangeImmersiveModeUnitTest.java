package de.deepwinter.systemuitweaker;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class ChangeImmersiveModeUnitTest {
    private final static String testPackage1 = "de.test.app1";
    private final static String testPackage2 = "com.test.app2";
    private final static String testPackage3 = "eu.test.app3";
    private final static String testValue = "immersive.full=de.test.app1,com.test.app2:immersive.status=com.test.app2,eu.test.app3:immersive.navigation=eu.test.app3";
    private final ArrayList<String> full;
    private final ArrayList<String> status;
    private final ArrayList<String> nav;

    public ChangeImmersiveModeUnitTest(){
        full = new ArrayList<>(2);
        full.add(testPackage1);
        full.add(testPackage2);
        status = new ArrayList<>(2);
        status.add(testPackage2);
        status.add(testPackage3);
        nav = new ArrayList<>(1);
        nav.add(testPackage3);
    }

    @Test
    public void CreateIsCorrect(){
        String val = ChangeImmersiveMode.ConcatAll(full,status,nav);
        assertEquals(testValue,val);
    }

    @Test
    public void ReadIsCorrect(){
        HashMap<String,ArrayList<String>> val = ChangeImmersiveMode.SplitAndAssign(testValue);
        ArrayList<String> fullActual = val.get(ChangeImmersiveMode.NAME_IMMERSIVE_FULL);
        assertEquals( full, fullActual );
        ArrayList<String> statusActual = val.get(ChangeImmersiveMode.NAME_IMMERSIVE_STATUS);
        assertEquals( status, statusActual );
        ArrayList<String> navActual = val.get(ChangeImmersiveMode.NAME_IMMERSIVE_NAVIGATION);
        assertEquals( nav, navActual );
    }
}
