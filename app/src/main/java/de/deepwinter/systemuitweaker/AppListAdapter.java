package de.deepwinter.systemuitweaker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import de.deepwinter.systemuitweaker.model.AndroidPackage;

class AppListAdapter extends BaseAdapter {
    private final Context context; //context
    private final ArrayList<AndroidPackage> items; //data source of the list adapter
    private boolean isInEditMode;

    //public constructor
    AppListAdapter(Context context, ArrayList<AndroidPackage> items) {
        this.context = context;
        this.items = items;
        isInEditMode = false;
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position); //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // inflate the layout for each list row
        if (convertView == null) {
            convertView = LayoutInflater.from(context).
                    inflate(R.layout.rowitem_applist, parent, false);
        }

        // get current item to be displayed
        AndroidPackage currentItem = (AndroidPackage) getItem(position);

        // get the TextView for item name and item description
        TextView textViewItemName = convertView.findViewById(R.id.tv_app_name);
        TextView textViewItemDescription = convertView.findViewById(R.id.tv_app_packagename);
        ImageView iv_app_icon = convertView.findViewById(R.id.iv_app_icon);

        CheckBox cb_immersivefull = convertView.findViewById(R.id.cb_immersivefull);
        CheckBox cb_immersivestatus = convertView.findViewById(R.id.cb_immersivestatus);
        CheckBox cb_immersvivenav = convertView.findViewById(R.id.cb_immersvivenav);

        cb_immersivefull.setEnabled(isInEditMode);
        cb_immersivestatus.setEnabled(isInEditMode);
        cb_immersvivenav.setEnabled(isInEditMode);

        cb_immersivefull.setChecked(items.get(position).isImmersiveFull());
        cb_immersivestatus.setChecked(items.get(position).isImmersiveStatus());
        cb_immersvivenav.setChecked(items.get(position).isImmersiveNav());

        cb_immersivefull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                items.get(position).setImmersiveFull(((CheckBox) view).isChecked());
            }
        });

        cb_immersivestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                items.get(position).setImmersiveStatus(((CheckBox) view).isChecked());
            }
        });

        cb_immersvivenav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                items.get(position).setImmersiveNav(((CheckBox) view).isChecked());
            }
        });

        //sets the text for item name and item description from the current item object
        textViewItemName.setText(currentItem.getAppName());
        textViewItemDescription.setText(currentItem.getPackageName());
        iv_app_icon.setImageDrawable(currentItem.getIcon());

        // returns the view for the current row
        return convertView;
    }

    public boolean isInEditMode() {
        return isInEditMode;
    }

    public void setisInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;
    }

    public ArrayList<AndroidPackage> getItems(){
        return items;
    }
}
