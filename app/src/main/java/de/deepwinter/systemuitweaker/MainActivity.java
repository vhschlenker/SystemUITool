package de.deepwinter.systemuitweaker;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private ChangeImmersiveMode changeImmersiveMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeImmersiveMode = new ChangeImmersiveMode(getApplicationContext().getContentResolver());

        setContentView(R.layout.activity_main);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        ViewPager viewPager = findViewById(R.id.viewpager);
        final SampleFragmentPagerAdapter mAdapter = new SampleFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                Fragment frag = mAdapter.fragments[position];
                if (frag != null && frag instanceof FragmentWithUpdate) {
                    ((FragmentWithUpdate)frag).updateStatus();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        // Give the TabLayout the ViewPager
        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    ChangeImmersiveMode getChangeImmersiveMode() {
        return changeImmersiveMode;
    }
}
