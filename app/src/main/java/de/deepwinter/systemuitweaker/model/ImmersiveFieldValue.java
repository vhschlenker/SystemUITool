package de.deepwinter.systemuitweaker.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import de.deepwinter.systemuitweaker.BR;

public class ImmersiveFieldValue extends BaseObservable {
    private String immersiveFieldValue;

    public ImmersiveFieldValue(String immersiveFieldValue) {
        this.immersiveFieldValue = immersiveFieldValue;
    }

    @Bindable
    public String getImmersiveFieldValue() {
        return immersiveFieldValue;
    }

    public void setImmersiveFieldValue(String immersiveFieldValue) {
        this.immersiveFieldValue = immersiveFieldValue;
        notifyPropertyChanged(BR.immersiveFieldValue);
    }
}
