package de.deepwinter.systemuitweaker.model;

import android.graphics.drawable.Drawable;

public class AndroidPackage {
    private final String appName;
    private final String packageName;
    private final Drawable icon;

    private boolean immersiveFull;
    private boolean immersiveStatus;
    private boolean immersiveNav;

    public AndroidPackage(String name, String packagename, Drawable icon) {
        this.appName = name;
        this.packageName = packagename;
        this.icon = icon;
    }

    public String getAppName() {
        return this.appName;
    }

    public String getPackageName() {
        return packageName;
    }

    public boolean isImmersiveFull() {
        return immersiveFull;
    }

    public void setImmersiveFull(boolean immersiveFull) {
        this.immersiveFull = immersiveFull;
    }

    public boolean isImmersiveStatus() {
        return immersiveStatus;
    }

    public void setImmersiveStatus(boolean immersiveStatus) {
        this.immersiveStatus = immersiveStatus;
    }

    public boolean isImmersiveNav() {
        return immersiveNav;
    }

    public void setImmersiveNav(boolean immersiveNav) {
        this.immersiveNav = immersiveNav;
    }

    public Drawable getIcon() {
        return icon;
    }
}
