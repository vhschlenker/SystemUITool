package de.deepwinter.systemuitweaker;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.deepwinter.systemuitweaker.model.AndroidPackage;

public class FragmentAppSpecific extends Fragment implements FragmentWithUpdate {
    private ChangeImmersiveMode changeImmersiveMode;
    private AppListAdapter appListAdapter;

    public static FragmentAppSpecific newInstance() {
        return new FragmentAppSpecific();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appspecific, container, false);

        // instantiate the custom list adapter
        appListAdapter = new AppListAdapter(getActivity(), getAllPackages());

        // get the ListView and attach the adapter
        final ListView itemsListView = view.findViewById(R.id.lv_applist);
        itemsListView.setAdapter(appListAdapter);

        final FloatingActionButton fab_edit = view.findViewById(R.id.fab_edit);
        fab_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchEditMode();
                Context context;
                if (getContext() != null) {
                    context = getContext();
                } else { return; }
                if(appListAdapter.isInEditMode()){
                    fab_edit.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_done_white_24dp));
                } else {
                    fab_edit.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_edit_white_24dp));
                }
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateStatus();
    }

    public void updateStatus() {
        readEntriesFromSetting();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            changeImmersiveMode = ((MainActivity) getActivity()).getChangeImmersiveMode();
        }
    }

    private ArrayList<AndroidPackage> getAllPackages() {
        PackageManager pm;
        ArrayList<AndroidPackage> itemsArrayList = new ArrayList<>();

        if(getActivity().getPackageManager() != null){
            pm = getActivity().getPackageManager();
        } else {
            return itemsArrayList;
        }
        List<ApplicationInfo> apps = pm.getInstalledApplications(0);
        for (ApplicationInfo app : apps) {
            //Only get apps with launcher intents
            if (pm.getLaunchIntentForPackage(app.packageName) != null) {
                itemsArrayList.add(new AndroidPackage((String) pm.getApplicationLabel(app), app.packageName, pm.getApplicationIcon(app)));
            }
        }

        return itemsArrayList;
    }

    private void switchEditMode(){
        if(appListAdapter.isInEditMode()){
            writeEntriesToSetting();
        }
        appListAdapter.setisInEditMode(!appListAdapter.isInEditMode());
        appListAdapter.notifyDataSetChanged();
    }

    private void writeEntriesToSetting(){
        ArrayList<String> retFull = new ArrayList<>();
        ArrayList<String> retStatus = new ArrayList<>();
        ArrayList<String> retNav = new ArrayList<>();
        for (AndroidPackage androidpackage : appListAdapter.getItems()) {
            if (androidpackage.isImmersiveFull()){
                retFull.add(androidpackage.getPackageName());
            }
            if (androidpackage.isImmersiveStatus()){
                retStatus.add(androidpackage.getPackageName());
            }
            if (androidpackage.isImmersiveNav()){
                retNav.add(androidpackage.getPackageName());
            }
        }
        changeImmersiveMode.SetImmersiveModePerApp(retFull,retStatus,retNav);
    }

    private void readEntriesFromSetting() {
        HashMap<String, ArrayList<String>> val = changeImmersiveMode.GetImmersiveModePerApp();
        ArrayList<String> retFull = val.get(ChangeImmersiveMode.NAME_IMMERSIVE_FULL);
        ArrayList<String> retStatus = val.get(ChangeImmersiveMode.NAME_IMMERSIVE_STATUS);
        ArrayList<String> retNav = val.get(ChangeImmersiveMode.NAME_IMMERSIVE_NAVIGATION);
        for (AndroidPackage androidpackage : appListAdapter.getItems()) {
            String packageName = androidpackage.getPackageName();
            androidpackage.setImmersiveFull(retFull.contains(packageName));
            androidpackage.setImmersiveStatus(retStatus.contains(packageName));
            androidpackage.setImmersiveNav(retNav.contains(packageName));
        }
        appListAdapter.notifyDataSetChanged();
    }
}
