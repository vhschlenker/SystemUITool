package de.deepwinter.systemuitweaker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
    private final String tabTitles[] = new String[]{"Global", "App spezifisch"};
    final Fragment[] fragments = new Fragment[tabTitles.length];

    SampleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return FragmentGlobal.newInstance();
        } else {
            return FragmentAppSpecific.newInstance();
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
        fragments[position]  = createdFragment;
        return createdFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
