package de.deepwinter.systemuitweaker;

interface FragmentWithUpdate {
    void updateStatus();
}
