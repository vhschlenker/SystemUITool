package de.deepwinter.systemuitweaker;

import android.content.ContentResolver;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

//From https://github.com/aosp-mirror/platform_frameworks_base/blob/master/services/core/java/com/android/server/policy/PolicyControl.java:

/**
 * Runtime adjustments applied to the global window policy.
 * <p>
 * This includes forcing immersive mode behavior for one or both system bars (based on a package
 * list) and permanently disabling immersive mode confirmations for specific packages.
 * <p>
 * Control by setting to one or more name-value pairs.
 * e.g.
 * to force immersive mode everywhere:
 * "immersive.full=*"
 * to force transient status for all apps except a specific package:
 * "immersive.status=apps,-com.package"
 * to disable the immersive mode confirmations for specific packages:
 * "immersive.preconfirms=com.package.one,com.package.two"
 * <p>
 * Separate multiple name-value pairs with ':'
 * e.g. "immersive.status=apps:immersive.preconfirms=*"
 */

class ChangeImmersiveMode {
    public static final String NAME_IMMERSIVE_FULL = "immersive.full";
    public static final String NAME_IMMERSIVE_STATUS = "immersive.status";
    public static final String NAME_IMMERSIVE_NAVIGATION = "immersive.navigation";
    public static final String NAME_IMMERSIVE_PRECONFIRMATIONS = "immersive.preconfirms";
    private static final String ALL = "*";
    private static final String EQUALS = "=";
    private static final String EXLUCDE = "-";
    private static final String NAME = "policy_control";
    private final ContentResolver contentResolver;

    ChangeImmersiveMode(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    void SetFullImmersiveMode() {
        PutString(NAME_IMMERSIVE_FULL + EQUALS + ALL);
    }

    void SetStatusImmersiveMode() {
        PutString(NAME_IMMERSIVE_STATUS + EQUALS + ALL);
    }

    void SetNavigationImmersiveMode() {
        PutString(NAME_IMMERSIVE_NAVIGATION + EQUALS + ALL);
    }

    void DisableImmersiveMode() {
        PutString("null*");
    }

    public String GetImmersiveModeStatus() {
        return Settings.Global.getString(contentResolver, NAME);
    }

    private void PutString(String value) {
        Settings.Global.putString(contentResolver, NAME, value);
    }

    public void SetImmersiveModePerApp(ArrayList<String> fullImmersivePackages, ArrayList<String> statusImmersivePackages, ArrayList<String> navigationImmersivePackages){
        PutString(ConcatAll(fullImmersivePackages,statusImmersivePackages,navigationImmersivePackages));
    }

    static String ConcatAll(ArrayList<String> fullImmersivePackages, ArrayList<String> statusImmersivePackages, ArrayList<String> navigationImmersivePackages){
        String fullImmersiveString = NAME_IMMERSIVE_FULL + EQUALS + concatStringWithComma(fullImmersivePackages);
        String statusImmersiveString = NAME_IMMERSIVE_STATUS + EQUALS + concatStringWithComma(statusImmersivePackages);
        String navigationImmersiveString = NAME_IMMERSIVE_NAVIGATION + EQUALS + concatStringWithComma(navigationImmersivePackages);
        return String.join(":",fullImmersiveString,statusImmersiveString,navigationImmersiveString);
    }

    private static String concatStringWithComma(ArrayList<String> strings){
        return String.join(",", strings);
    }

    private static ArrayList<String> splitAndConvertToArray(String string){
        return new ArrayList<>(Arrays.asList(string.split(",")));
    }

    public HashMap<String,ArrayList<String>> GetImmersiveModePerApp() {
        return SplitAndAssign(GetImmersiveModeStatus());
    }

    static HashMap<String,ArrayList<String>> SplitAndAssign(String value) {
        String[] singelVals = value.split(":");
        String fullImmersiveString="";
        String statusImmersiveString="";
        String navigationImmersiveString="";

        for (String val: singelVals) {
            if (val.startsWith(NAME_IMMERSIVE_FULL)){
                fullImmersiveString = val.replace(NAME_IMMERSIVE_FULL + EQUALS,"");
            } else if (val.startsWith(NAME_IMMERSIVE_NAVIGATION)){
                navigationImmersiveString = val.replace(NAME_IMMERSIVE_NAVIGATION + EQUALS,"");
            } else if (val.startsWith(NAME_IMMERSIVE_STATUS)) {
                statusImmersiveString = val.replace(NAME_IMMERSIVE_STATUS + EQUALS,"");
            } else {
                Log.d("tmp", "unknown string: " + val);
            }
        }

        HashMap<String,ArrayList<String>> ret = new HashMap<>(3);
        ret.put(NAME_IMMERSIVE_FULL,splitAndConvertToArray(fullImmersiveString));
        ret.put(NAME_IMMERSIVE_NAVIGATION,splitAndConvertToArray(navigationImmersiveString));
        ret.put(NAME_IMMERSIVE_STATUS,splitAndConvertToArray(statusImmersiveString));
        return ret;
    }
}
