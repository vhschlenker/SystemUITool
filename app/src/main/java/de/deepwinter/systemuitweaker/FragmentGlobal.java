package de.deepwinter.systemuitweaker;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.deepwinter.systemuitweaker.databinding.FragmentGlobalBinding;
import de.deepwinter.systemuitweaker.model.ImmersiveFieldValue;

public class FragmentGlobal extends Fragment implements FragmentWithUpdate {
    private ChangeImmersiveMode changeImmersiveMode;
    private ImmersiveFieldValue immersiveFieldValue;

    public static FragmentGlobal newInstance() {
        return new FragmentGlobal();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        FragmentGlobalBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_global, container, false);

        immersiveFieldValue = new ImmersiveFieldValue("Empty");
        mBinding.setImmersiveFieldValue(immersiveFieldValue);

        mBinding.btnImmersivefull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeImmersiveMode.SetFullImmersiveMode();
                updateStatus();
            }
        });

        mBinding.btnImmersivenav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeImmersiveMode.SetNavigationImmersiveMode();
                updateStatus();
            }
        });

        mBinding.btnImmersivestatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeImmersiveMode.SetStatusImmersiveMode();
                updateStatus();
            }
        });

        mBinding.btnDisableimmersive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeImmersiveMode.DisableImmersiveMode();
                updateStatus();
            }
        });

        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateStatus();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null) {
            changeImmersiveMode = ((MainActivity) getActivity()).getChangeImmersiveMode();
        }
        updateStatus();
    }

    public void updateStatus() {
        immersiveFieldValue.setImmersiveFieldValue(changeImmersiveMode.GetImmersiveModeStatus());
    }
}
